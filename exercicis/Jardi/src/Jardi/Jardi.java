package Jardi;

public class Jardi{
	private Planta jardi[];
	
	public Jardi(int mida) {
		if (mida<1){
			mida=10;
			}
		jardi = new Planta[mida];
	}
	public void temps(){
			int i = 0;	 	
			for (Planta p : jardi) {	 	
				if (p != null) {	 	
					Planta p1 = p.creix();	 	
					int pos;	 	
					if(p1 instanceof Llavor ) {	 	
							pos = p.escampaLlavor()+i;	 	
								plantaLlavor(p1,pos);	 	
					}	 	
					else if (p1 instanceof Planta ) {	 	
						jardi[i] = p1;	 	
					}	 	
					else if (p.esViva()==false)	 	
						jardi[i] = null;	 	
				}	 	
				i++;	 	
			}	
		}
	@Override
	public String toString() {

		StringBuilder str = new StringBuilder();

		for(int i = 10; i >= 0; i--) {

			for(int j = 0; j < jardi.length; j++) {

				if (jardi[j] != null) 
					str.append(jardi[j].getChar(i));
				else
					str.append(' ');

			}

			str.append('\n');
		}
		for(int i = 0; i < jardi.length; i++) str.append('_');
		str.append('\n');

		return str.toString();
	}
	
	boolean  plantaLlavor(Planta novaPlanta, int pos) {
		if(pos>=0 && pos<jardi.length){
			if(jardi[pos]== null){
					jardi[pos] = novaPlanta;
					return true;
				}
			}
			return false;
		}
}
