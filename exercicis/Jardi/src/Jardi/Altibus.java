package Jardi;

public class Altibus extends Planta {

	public Llavor creix(){
		if(altura>7){
			Planta p = new Altibus();
			Llavor ll =new Llavor(p);
			return ll;
		}else
			return null;		
	}

	@Override
	public char getChar(int nivell) {
		if(altura==nivell){
			return 'O';
		}else if(altura<nivell){
			return '|';
		}else
			return ' ';
	}

}
