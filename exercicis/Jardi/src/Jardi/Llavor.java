package Jardi;

public class Llavor extends Planta{
	protected Planta planta;
	protected int temps=0;
	public Llavor(Planta planta){
		planta = this.planta;
		if(planta instanceof Llavor){
			throw new IllegalArgumentException("Error");
		}
	}
	public Planta creix(){
		if(temps<5){
			temps++;
			return null;
		}else
			return planta;
	}
	@Override
	public char getChar(int nivell) {
		if(altura==0){
			return '.';
		}else
			return ' ';
	}
}
