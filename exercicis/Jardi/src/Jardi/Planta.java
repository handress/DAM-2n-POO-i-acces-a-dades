package Jardi;

public abstract class Planta {
	boolean esViva=true;
	int altura;
	
	public abstract char getChar(int nivell);
	public Planta creix(){
		if(altura==10){
			esViva=false;
		}else{
			altura++;}
		return null;
	}
	public int escampaLlavor(){
		int escampar[]={-2,-1,1,2};
		int val=(int) Math.floor(Math.random()*4);
		return escampar[val];
	}
	public int getAltura(){
		return altura;
	}
	public boolean esViva(){
		return esViva;
	}
}