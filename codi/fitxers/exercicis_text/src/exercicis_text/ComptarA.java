package exercicis_text;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ComptarA {
	public static void main(String[] args) {
		int n;
		int numA = 0;
		String nomFitxer;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nom de fitxer: ");
		nomFitxer = sc.nextLine();
		try (FileReader lector = new FileReader(nomFitxer)) {
			while ((n = lector.read()) != -1) {
				char c = (char) n;
				if (Character.toLowerCase(c) == 'a')
					numA++;
			}
			System.out.println("Al fitxer hi ha " + numA + " lletres 'a'");
		} catch (FileNotFoundException e) {
			System.err.println("No s'ha trobat el fitxer");
		} catch (IOException e) {
			System.err.println("No es pot llegir el fitxer: " + e.getMessage());
		}
		sc.close();
	}

}
