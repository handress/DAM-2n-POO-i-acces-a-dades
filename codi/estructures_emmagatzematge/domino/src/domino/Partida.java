package domino;

import java.util.HashMap;
import java.util.Map;

/**
 * La classe Partida permet coordinar els diversos elements que
 * composen una partida de domino: el domino amb totes les fitxes,
 * els jugador, i les fitxes que hi ha damunt la taula.
 */
public class Partida {
	/**
	 * Fitxes que queden per agafar.
	 */
	private Domino domino = new Domino();
	/**
	 * Els jugadors de la partida.
	 */
	private Jugador[] jugadors;
	/**
	 * Les fitxes que ja s'han jugat.
	 */
	private Taula taula = new Taula();
	/**
	 * Índex del jugador que té el torn.
	 */
	private int torn = 0;
	
	/**
	 * El constructor rep la quantitat de jugadors de la partida
	 * i crea els jugadors.
	 * 
	 * @param numJugadors  Quants jugadors juguen aquesta partida.
	 */
	public Partida(int numJugadors) {
		jugadors = new Jugador[numJugadors];
		for (int i=0; i<numJugadors; i++) {
			jugadors[i] = new Jugador(""+(i+1), domino);
		}
	}
	/**
	 * Retorna el jugador que té el torn actualment.
	 * 
	 * @return el jugador a qui li toca jugar.
	 */
	public Jugador jugadorTorn() {
		return jugadors[torn];
	}
	/**
	 * Passa el torn al següent jugador.
	 */
	public void seguentTorn() {
		torn = (torn+1)%jugadors.length;
	}
	/**
	 * Obté l'índex del jugador que té el torn actualment.
	 * 
	 * @return el número de jugador que té el torn.
	 */
	public int getTorn() {
		return torn;
	}
	/**
	 * Fa que el jugador que té el torn agafi una fitxa de
	 * les que queden al domino. Si no pot agafar una fitxa
	 * perquè ja no en queden, el torn passa al jugador
	 * següent.
	 * 
	 * @return true si el jugador ha agafat fitxa, false si
	 * no ha pogut agafar-ne i ha perdut el torn.
	 */
	public boolean agafa() {
		boolean potAgafar = false;
		
		if (domino.mida() > 0) {
			jugadorTorn().agafa(domino.agafaFitxa());
			potAgafar = true;
		} else {
			seguentTorn();
		}
		return potAgafar;
	}
	
	/**
	 * Comprova si un jugador té almenys una jugada legal, independentment
	 * que sigui el seu torn o no.
	 * 
	 * @param jugador  El jugador pel qual es vol mirar si pot jugar alguna fitxa.
	 * @return true si el jugador pot jugar, false si no pot jugar.
	 */
	public boolean potJugar(Jugador jugador) {
		boolean pot = false;
		for (Fitxa f : jugador.getFitxes()) {
			if (tiradesPossibles(f)>0)
				pot = true;
		}
		return pot;
	}
	/**
	 * Calcula de quantes formes diferents es pot jugar una
	 * determinada fitxa.
	 * 
	 * @param f  La fitxa que es vol comprovar.
	 * @return un nombre entre 0 i 2, indicant de quantes formes
	 * es pot jugar f.
	 */
	public int tiradesPossibles(Fitxa f) {
		int opcions = 0;
		
		if (taula.mida() == 0) {
			opcions = 1;
		} else {
			if (f.encaixa(taula.getNum(Posicio.ESQUERRA))!=Posicio.CAP)
				opcions++;
			if (f.encaixa(taula.getNum(Posicio.DRETA))!=Posicio.CAP)
				opcions++;
		}
		return opcions;
	}
	/**
	 * Intenta jugar una fitxa a un dels extrems de la taula. Si la fitxa
	 * es pot jugar, es treu la fitxa del jugador i es posa a la taula, a
	 * la posició indicada, i el torn passa al jugador següent.
	 * 
	 * @param f  La fitxa que es vol jugar.
	 * @param pos  La posició on es vol jugar la fitxa, DRETA o ESQUERRA.
	 * @return true si s'ha pogut jugar la fitxa a la posició indicada,
	 * false en cas contrari.
	 * @throws IllegalArgumentException si es passa la posició CAP.
	 */
	public boolean juga(Fitxa f, Posicio pos) throws IllegalArgumentException {
		if (pos == Posicio.CAP)
			throw new IllegalArgumentException("La posició ha de ser dreta o esquerra");
		boolean potJugar = false;
		if (jugadorTorn().te(f)) {
			if (taula.mida()==0 || f.encaixa(taula.getNum(pos)) != Posicio.CAP) {
				potJugar=true;
			}
		}
		if (potJugar) {
			taula.afegeix(f, pos);
			jugadorTorn().juga(f);
			seguentTorn();
		}
		return potJugar;
	}
	
	/**
	 * Intenta jugar una fitxa a qualsevol dels extrems de la taula.
	 * Si la fitxa es pot jugar, es treu la fitxa del jugador i es
	 * posa a la taula, a la posició indicada, i el torn passa al
	 * jugador següent.
	 * 
	 * @param f  La fitxa que es vol jugar.
	 * @return true si s'ha pogut jugar la fitxa, false en cas contrari.
	 */
	public boolean juga(Fitxa f) {
		boolean potJugar = juga(f, Posicio.DRETA);
		if (!potJugar)
			potJugar = juga(f, Posicio.ESQUERRA);
		return potJugar;
	}
	/**
	 * Comprova si s'ha arribat al final de la partida. La partida acaba
	 * quan:
	 * 
	 * - un jugador no té cap fitxa, o
	 * - cap jugador pot tirar i ja no queden fitxes per agafar.
	 * 
	 * @return true si s'ha acabat la partida, false en cas contrari.
	 */
	public boolean finalPartida() {
		boolean acabada = true;
		if (domino.mida()==0) {
			for (Jugador j : jugadors) {
				if (potJugar(j)) {
					acabada = false;
				}
			}
		} else
			acabada = false;
		if (!acabada) {
			for (Jugador j : jugadors) {
				if (j.numFitxes() == 0) {
					acabada = true;
				}
			}
		}
		return acabada;
	}
	/**
	 * Retorna un diccionari amb una relació dels jugador i
	 * les puntuacions que tenen. La puntuació de cada jugador
	 * és la suma dels valors de les seves fitxes, i com més
	 * petita és millor.
	 * 
	 * @return Un diccionari que relaciona cada jugador amb
	 * la seva puntuació.
	 */
	public Map<Jugador, Integer> getPuntuacions() {
		Map<Jugador, Integer> puntuacions = new HashMap<>();
		for (Jugador j : jugadors) {
			puntuacions.put(j, j.getPuntuacio());
		}
		return puntuacions;
	}
	/**
	 * Retorna una cadena amb una representació de les fitxes que
	 * ja s'han jugat.
	 * 
	 *  @return una cadena amb les fitxes que hi ha a la taula.
	 */
	@Override
	public String toString() {
		return taula.toString();
	}
}
