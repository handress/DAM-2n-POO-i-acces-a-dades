package cerca_binaria.c;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LlistaOrdenada {
	private List<Integer> llista = new ArrayList<Integer>();
	
	public int binarySearch(Integer element) {
		int pos = -1;
		int inici = 0;
		int fi = llista.size() - 1;
		int mig;
		Integer elementMig;
		while (fi - inici >= 0) {
			mig = (inici + fi) / 2;
			elementMig = llista.get(mig);
			if (elementMig.equals(element)) {
				pos = mig;
				inici = fi + 1;
			} else if (element < elementMig) {
				fi = mig - 1;
			} else { // element > elementMig
				inici = mig + 1;
			}
		}
		
		return pos;
	}
	
	public void add(Integer element) {
		int pos = 0;
		Integer n;
		Iterator<Integer> it = llista.iterator();
		boolean sortida = false;
		while (it.hasNext() && !sortida) {
			n = it.next();
			if (n >= element) {
				sortida = true;
			} else
				pos++;
		}
		llista.add(pos, element);
	}
	
	public Integer get(int index) {
		return llista.get(index);
	}
	
	public boolean remove(Integer element) {
		return llista.remove(element);
	}
	
	public int size() {
		return llista.size();
	}
}
