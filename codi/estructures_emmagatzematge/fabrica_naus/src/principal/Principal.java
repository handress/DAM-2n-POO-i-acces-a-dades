package principal;

import components_compostos.Hangar;
import components_compostos.Nau;
import components_simples.Habitacle;
import components_simples.Motor;

public class Principal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Nau nau = new Nau("Creuer interestel·lar Aníbal", 10, 1000);
		Motor m1 = new Motor(2500, 50);
		nau.add(m1);
		Motor m2 = new Motor(1500, 50);
		m2.setNom("Motor auxiliar");
		nau.add(m2);
		Habitacle hab = new Habitacle(50, 120);
		nau.add(hab);
		Hangar hangar = new Hangar(200, 80);
		Nau caza = new Nau("Caça cicló", 2, 75);
		Motor m3 = new Motor(200, 30);
		caza.add(m3);
		hangar.add(caza);
		nau.add(hangar);
		
		System.out.println(nau.descriu());
		System.out.println("Potència total: "+nau.getPotenciaTotal());
		System.out.println("Pes total: "+nau.getPesTotal());
		System.out.println();
		System.out.println(caza.descriu());
		System.out.println("Potència total: "+caza.getPotenciaTotal());
		System.out.println("Pes total: "+caza.getPesTotal());
	}
}
