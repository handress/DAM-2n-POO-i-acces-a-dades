package consultes_estatiques;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*4. Fes un programa que mostri 
 * totes les botigues que hi ha, 
 * amb les seves adreces i el nom i cognom del seu encarregat.
 */
public class Ex4 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "usbw";
		String sql = "SELECT s.store_id, a.address, st.first_name, st.last_name" +
				" FROM store s"+
				" JOIN staff st ON st.staff_id=s.manager_staff_id"+
				" JOIN address a ON a.address_id=s.address_id";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(sql);) {
			while (rs.next())
				System.out.print("Store_ID: "+rs.getString(1)+"\n Address: "+rs.getString(2)+"\n Staff:\n  FirstName: "+rs.getString(3)+"\n  LastName: "+rs.getString(4)+"\n");
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
