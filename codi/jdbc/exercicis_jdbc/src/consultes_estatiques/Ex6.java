package consultes_estatiques;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
6- Fes un programa que mostri les diferents categories de pel·lícules que tenim
i quantes pel·lícules de cada categoria hi ha.
*/
public class Ex6 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "super";
		String sql = "SELECT c.id, c.name, count(*) as n"
				+ " FROM categories c"
				+ " JOIN film_category fc USING(category_id)"
				+ " JOIN film f USING(film_id)"
				+ " GROUP BY c.id"
				+ " ORDER BY c.id";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(sql);) {			
			while (rs.next()){
				System.out.println("Categoria "+rs.getString(2)+" ("+
						rs.getInt(1)+"): "+rs.getInt(3)+" pel·lícules.");
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
