package examenJDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Inventory {
	public final int id;
	public final int filmId;
	public final int storeId;
	
	public Inventory(int id, int filmId, int storeId) {
		this.id = id;
		this.filmId = filmId;
		this.storeId = storeId;
	}

	public static Inventory byId(int id) throws SQLException {
		Inventory inventory = null;
		try (
			PreparedStatement st = DB.connection().prepareStatement("select inventory_id, film_id, store_id from inventory where inventory_id = ?");
		) {
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				inventory = new Inventory(rs.getInt(1), rs.getInt(2), rs.getInt(3));
			}
			rs.close();
			return inventory;
		}
	}
	
	public Film film() throws SQLException {
		return Film.byId(filmId);
	}
	
	public String toString() {
		return id+" "+filmId+" "+storeId;
	}
}
