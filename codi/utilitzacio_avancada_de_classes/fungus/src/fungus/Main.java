package fungus;

import java.util.Arrays;
import java.util.Collections;

public class Main {

	public static void main(String[] args) {
		Colonia[] colonies = new Colonia[3];
		colonies[0] = new Colonia("compactus");
		colonies[1] = new Colonia("dispersus");
		colonies[2] = new Colonia("agresibus");
		Cultiu cultiu = new Cultiu(10, 10);
		cultiu.setFungus(new Compactus(colonies[0]), 2, 4);
		cultiu.setFungus(new Dispersus(colonies[1]), 6, 6);
		cultiu.setFungus(new Agresibus(colonies[2]), 1, 2);
		cultiu.dibuixa();
		for (int i=0; i<100; i++) {
			cultiu.temps();
		}
		cultiu.dibuixa();
		Arrays.sort(colonies, Collections.reverseOrder());
		for (Colonia c : colonies)
			System.out.println(c);
	}

}
