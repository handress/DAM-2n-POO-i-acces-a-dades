package ex2;

public class ProvaSeries {

	public static void main(String[] args) {
		Serie s1 = new PerDos();
		Serie s2 = new NegEntreDos();
		
		primersNombres(s1, 5);
		primersNombres(s2, 2);
	}

	/**
	 * Mètode per calcular i mostrar per pantalla els 20
	 * primers nombres d'una sèrie qualsevol.
	 * 
	 * @param s  La sèrie de la qual es volen calcular elements.
	 * @param llavor  La llavor utilitzada per inicialitzar la sèrie.
	 */
	public static void primersNombres(Serie s, int llavor) {
		s.inicialitza(llavor);
		
		for (int i=0; i<20; i++)
			System.out.println(i+": "+s.seguent());
	}
}
