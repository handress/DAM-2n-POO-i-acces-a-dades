package exemple1;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Random;

/**
 * Per ordenar els vols depenent de l'hora de sortida o de l'hora
 * d'arribada, creem dos comparadors.
 * 
 * En aquest exemple, s'han implementat els comparadors a les classes
 * ComparadorArribades i ComparadorSortides, i s'ha creat un objecte
 * de classe a l'hora d'ordenar.
 *
 */
public class Main {
	private static final int N_VOLS = 10;
	private static final Random random = new Random();
	private VolReal[] vols = new VolReal[N_VOLS];

	public static void main(String[] args) {
		Main main = new Main();
		main.mostraVols();
		// Ordenem per sortides
		/* Creem un objecte de la classe ComparadorSortides per dir-li a
		 * Arrays.sort com ha de fer l'ordenació.
		 */
		ComparadorSortides comparadorSortides = new ComparadorSortides();
		Arrays.sort(main.vols, comparadorSortides);
		main.mostraVols();
		// Ordenem per arribades
		/*
		 * El mateix que en el cas de les sortides, però hem compactat una
		 * mica el codi, aprofitant que el comparador només s'utilitza per
		 * passar-lo a sort.
		 */
		Arrays.sort(main.vols, new ComparadorArribades());
		main.mostraVols();
	}
	
	public Main() {
		/*
		 * La classe ZonedDateTime representa un moment en el temps en un fus 
		 * horari concret. A partir de Java 8.
		 */
		ZonedDateTime sortida, arribada;
		
		for (int i=0; i<vols.length; i++) {
			// Els vols sortiran en un temps aleatori durant la propera hora.
			// now() agafa el moment actual, sumem entre 0 i 59 minuts, i eliminem la informació
			// de temps de més precisió que minuts.
			sortida = ZonedDateTime.now().plusMinutes(random.nextInt(60)).truncatedTo(ChronoUnit.MINUTES);
			// L'hora d'arribada serà en algun moment entre la sortida i les següents 10 hores.
			arribada = sortida.plusMinutes(random.nextInt(10*60));
			vols[i] = new VolReal(sortida, arribada);
		}
	}
	
	public void mostraVols() {
		for (VolReal v : vols) 
			System.out.println(v.getSortida()+" - "+v.getArribada());
		System.out.println();
	}
}
