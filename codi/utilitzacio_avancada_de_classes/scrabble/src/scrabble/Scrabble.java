package scrabble;

public class Scrabble implements Comparable<Scrabble> {
	private String paraula;
	
	public Scrabble(String paraula) throws IllegalArgumentException {
		for (char ch : paraula.toCharArray()) {
			if (!Character.isLetter(ch))
				throw new IllegalArgumentException("A la paraula només hi poden haver lletres");
		}			
		this.paraula = paraula.toUpperCase();
	}
	
	public String getParaula() {
		return paraula;
	}
	
	public int valor() {
		int v = 0;
		
		if (paraula != null) {
			for (char ch : paraula.toCharArray()) {
				v += valorDeLletra(ch);
			}
		}
		return v;
	}
	
	// També s'hauria pogut fer amb un HashMap
	public static int valorDeLletra(char ch) {
		switch (ch) {
		case 'D': case 'G':
			return 2;
		case 'B': case 'C': case 'M': case 'P':
			return 3;
		case 'F': case 'H': case 'V': case 'W': case 'Y':
			return 4;
		case 'K':
			return 5;
		case 'J': case 'X':
			return 8;
		case 'Q': case 'Z':
			return 10;
		default:
			return 1;
		}
	}
	
	@Override
	public int compareTo(Scrabble s) {
		return valor() - s.valor();
	}
	
	@Override
	public String toString() {
		return "["+paraula+" - "+valor()+"]";
	}
}
