package scrabble;

import java.util.Comparator;

public class ComparadorScrabble implements Comparator<String>{

	private static ComparadorScrabble COMPARADOR_SCRABBLE = null;

	public static ComparadorScrabble getInstance() {
		if (COMPARADOR_SCRABBLE == null)
			COMPARADOR_SCRABBLE = new ComparadorScrabble();
		return COMPARADOR_SCRABBLE;
	}

	private ComparadorScrabble() {
	}

	public static int valor(String s) {
		int punts = 0;

		for (char ch : s.toUpperCase().toCharArray())
			punts += valorDeLletra(ch);

		return punts;
	}

	@Override
	public int compare(String s1, String s2) {
		return valor(s1) - valor(s2);
	}

	public static int valorDeLletra(char ch) {
		switch (ch) {
		case 'D': case 'G':
			return 2;
		case 'B': case 'C': case 'M': case 'P':
			return 3;
		case 'F': case 'H': case 'V': case 'W': case 'Y':
			return 4;
		case 'K':
			return 5;
		case 'J': case 'X':
			return 8;
		case 'Q': case 'Z':
			return 10;
		default:
			return 1;
		}
	}
}
