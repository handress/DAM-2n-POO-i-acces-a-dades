### Interfícies, implementacions i algorismes

En les estructures dinàmiques, habitualment distingirem quines
operacions volem que la nostra estructura suporti i què fa cadascuna
d'elles de la forma com les implementarem internament. D'aquesta manera,
qui utilitzi la nostra estructura no ha de conèixer els detalls
d'implementació, sinó que en té prou amb saber com utilitzar-la. A més,
si tenim diverses formes d'implementar la mateixa estructura, podem
deixar clar que es tracta de la mateixa estructura implementada de forma
diferent, si les dues implementacions segueixen la mateixa interfície.
La programació orientada a objectes té com un dels seus pilars
fonamentals l'ocultació dels detalls d'implementació del que és la
interfície visible d'una classe.

El llenguatge de programació ens permet fer aquesta separació no
solament a nivell conceptual, sinó també a nivell de codi: veurem que
podem implementar la mateixa estructura de diverses formes i que es
manté la relació entre les dues implementacions a través d'una
interfície comuna.

Després, estudiarem algunes classes que ens ofereixen organismes
genèrics per a moltes estructures, com la seva ordenació, o la conversió
d'un tipus d'estructura en un altre.

En Java, una interfície es defineix de manera molt similar a una classe:
s'utilitza la paraula clau *interface* en comptes de *class*, i
s'escriuen les declaracions dels mètodes, és a dir, el nom, valor de
retorn i paràmetres, però en comptes de posar-hi el codi corresponent,
s'acaba la declaració amb un punt i coma.

Per exemple, la declaració d'una interfície *Volador* tindria un aspecte
com aquest:

```java
public interface Volador {
   public void enlaire();
   ...
}
```

Quan tinguem clar com volem que sigui la interfície, podem passar a la
implementació pròpiament dita. Això es farà de la següent manera:

```java
public class Ocell implements Volador {
   public void enlaire() {
       // codi
   }
}
```

Quan diem que una classe “*implements Volador*”, el compilador ens
obliga a tenir els mètodes que hem declarat a la interfície i a
posar-los algun codi a dins.

Fixeu-vos que seria possible que, per una banda, la classe *Ocell*
derivés d'*Animal* i implementés *Volador*, i pel l'altra tinguéssim la
classe *Avio*, que derivaria de *Vehicle* i també implementaria
*Volador*.
