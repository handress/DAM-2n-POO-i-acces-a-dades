#### Conjunts

Un **conjunt** és una estructura dinàmica que conté una sèrie d'elements no
repetits i desordenats, és a dir, els elements d'un conjunt no ocupen
una posició o índex concret dins del conjunt, sinó que diem que un
element és al conjunt o no és al conjunt.

En Java, els conjunts es defineixen a la interfície *Set*, amb mètodes
com *add* per afegir elements, *remove* per extreure'ls, o *contains*
per comprovar si un element és al conjunt o no. Els elements d'un
conjunt també es poden recórrer amb un iterador o un bucle for-each,
però no podem saber en quin ordre es farà aquest recorregut, només que
es passarà un cop per cada element.

Els conjunt es defineixen a la interfície *Set*, i la seva implementació
més genèrica és fa a la classe *HashSet*. També existeixen la interfície
*SortedSet* i la classe *TreeSet*, que funcionen com conjunts en el
sentit que els elements no es poden repetir, però que a més mantenen els
elements ordenats segons el seu ordre natural (per exemple, un conjunt
ordenat d'enters ens retornaria els elements ordenats de menor a major).
