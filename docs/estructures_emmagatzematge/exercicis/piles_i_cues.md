#### Exercicis de piles i cues

1. Tenim una pila de nombres enters sobre la qual es realitzen una sèrie
d'operacions. Digues què sortirà per pantalla:

 ```
 push(2)
 push(6)
 push(3)
 i=pop()
 print i
 push(5)
 push(4)
 i=pop()
 print i
 push(7)
 mentre hi hagi elements:
     i=pop()
     print i
 ```

* Ara fem les mateixes operacions sobre una cua. Què sortirà per pantalla?

* Escriu un programa que utilitzi una pila per invertir una cadena.

* Escriu un programa que rebi nombres de l'usuari fins que un d'ells sigui
0 i que després els mostri tots:

  * en el mateix ordre com s'han introduït.
  * en ordre invers a com s'han introduït.

* Escriu un programa que llegeixi un enter positiu i escrigui la
representació binària d'aquest enter. Pista: divideix l'enter per 2.

* Escriviu un programa que utilitzi l'estructura pila per processar els
caràcters d'una expressió introduïda per teclat i que verifiqui:

 * L'equilibri de parèntesis (), claus {} i claudàtors []. Per cada un
    d'obert n'hi ha d'haver un de tancat i en l'ordre correcte.

    Per exemple:

    `2*[(a+b)/5]-7` és una expressió correcta.

    `2*[(a+b)/5-7` és incorrecta.

  * Que no hi hagi símbols mal niats.

    Per exemple:

    `2*[(a+b)/5]-7` és correcta.

    `2*[(a+b]/5)-7` és incorrecta.

 El programa mostrarà un missatge dient que l'expressió és correcta o bé
mostrarà l'expressió indicant el punt on hi ha l'error i una explicació
de l'error.

 Per exemple:

 ```
 2*[(a+b]/5)-7
         ^
 Error: s'esperava ) i s'ha trobat ].
 ```

 ***Pista***: la idea és utilitzar una pila on s'aniran guardant els símbols
especials que trobem i d'on es recuperaran quan es trobi el símbol de
tancar corresponent.
