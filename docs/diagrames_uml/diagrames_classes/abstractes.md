## Interfícies i classes abstractes

Les **interfícies** són un tipus de classe que no tenen implementació.
S'utilitza com a sistema per imposar un cert contracte, és a dir,
qualsevol classe que implementi una interfície ha d'implementar
obligatòriament (o deixar com abstractes) els seus mètodes. Amb això
s'aconsegueix que els objectes de qualsevol d'aquestes classes siguin
intercanviables entre ells.

De forma excepcional, a partir de la versió 8 del Java, existeixen
algunes interfícies que tenen codi per algun dels seus mètodes.

Les **classes abstractes** són classes de les quals no es poden
instanciar directament objectes. Per tal que un objecte sigui del tipus
d'una classe abstracta cal que es creï com a instància d'alguna de les
classes que deriven de la classe abtracta.

Un exemple clàssic de classe abstracta és la classe *Animal*. D'*Animal*
en poden derivar diverses classes: *Gos*, *Gat*, *Formiga*, etc. No té
sentit crear un animal que no tingui un tipus més concret, per això *Animal*
ha de ser abstracta. Quan creem un objecte de tipus *Gos*, aquest objecte
és també de tipus *Animal*.

Molt habitualment les classes abstractes inclouen alguns mètodes abstractes.
Els **mètodes abstractes** no tenen implementació. Les classes que deriven
d'una classe abstracte estan obligades a proporcionar el codi d'aquests
mètodes (o han de ser també abstractes).

Les *interfícies* es denoten amb la paraula clau *interface*, o amb
la lletra "I". Les classes i mètodes abstractes es mostren en itàliques,
o amb la paraula clau *abstract*.

Per dir que una classe implementa una interfície s'utilitza la mateixa
notació que per l'herència, però amb la línia puntejada. Per indicar que
una interfície amplia una altra interfície s'utilitza la mateixa notació
que per a l'herència de classes.

![Exemple interfície](docs/diagrames_uml/imatges/exemple_interficie.png)