## Anotacions

És habitual afegir anotacions als diagrames UML per tal de fer notar els
detalls que volem destacar del diagrama.

![Exemple anotació](docs/diagrames_uml/imatges/exemple_anotacio.png)