## Exercici de l'Scrabble

En el joc de l'Scrabble, un jugador crea paraules a partir d'un conjunt de
lletres que té. El valor de la paraula depèn, entre d'altres coses, de la
suma dels valors de cadascuna de les lletres. El valor de cada lletra és
fixat i depèn de si la lletra és o no és comuna.

En la versió anglesa del joc el valor de les lletres és el següent:

| Punts    |              1               |  2   |     3      |       4       |  5  |  8   |  10  |
| -------- | ---------------------------- | ---- | ---------- | ------------- | --- | ---- | ---- |
| Lletres  | A, E, I, L, N, O, R, S, T, U | D, G | B, C, M, P | F, H, V, W, Y |  K  | J, X | Q, Z |

Per exemple, la paraula "JAVA" tindria un valor de 8+1+4+1=14 punts.

### Primera part. Classe *Scrabble*.

Els objectes de la classe *Scrabble* representen una paraula en el joc.

a) Implementa la classe *Scrabble* amb un mètode `int valor()` que retorni
els punts que val la paraula que representa l'objecte.

b) Fes que tingui també un constructor que rebi la paraula com a paràmetre.
Aquest constructor llançarà una excepció de tipus *IllegalArgumentException*
si la cadena conté espais o símbols de puntuació. Pista: `Character.isLetter()`.

c) Fes que la classe *Scrabble* implementi la interfície *Comparable*. El
resultat a de permetre identificar quina paraula entre dues té més puntuació.

d) Sobreescriu el mètode *toString* per tal que es retorni la paraula
juntament amb el seu valor.

### Segona part. Classe *ComparadorScrabble*.

Per resoldre el problema anterior, una altra opció hagués estat no
implementar una classe *Scrabble* específica i emmagatzemar les paraules
en *String* directament. Per comparar-les, hauríem de crear una classe
*ComparadorScrabble* que implementés *Comparator* i permetés comparar dues
cadenes segons aquest criteri.

Crea aquesta classe amb un mètode *valor* que retorni la puntuació d'una
paraula, i el mètode *compare* de *Comparator*.

Dins d'aquesta classe crea la variable *COMPARADOR_SCRABBLE*, de tipus
*ComparadorScrabble* i inicialitza-la a *null*.

Fes el mètode *getInstance()* que creï un *ComparadorScrabble* a l'atribut
*COMPARADOR_SCRABBLE* si encara no n'hi ha cap. Retorna aquest objecte.

Fes que el constructor de *ComparadorScrabble* sigui *private*, per evitar que
es puguin crear més objectes d'aquesta classe.

### Tercera part. Classe *ScrabbleEx*.

Realment, la puntuació d'una paraula d'Scrabble depèn tant de les lletres
de la paraula com de la casella on es posen: hi ha caselles que dupliquen
o tripliquen el valor de la lletra que s'hi posa, i hi ha caselles que
dupliquen o tripliquen el valor de tota la paraula.

a) Crea la classe *Casella* que permeti guardar una lletra i els modificadors
de la casella on es troba. Crea el constructor i mètodes *get* i *set*
necessaris.

b) Fes que *Casella* implementi *Cloneable* per tal que es pugin fer còpies
profundes d'una casella.

c) Crea la classe *ScrabbleEx* que contingui un vector de caselles. Crea
un constructor que rebi un vector de caselles, que per simplificar suposarem
que ja està correctament inicialitzat.

d) Fes el mètode *valor* a *ScrabbleEx* que retorni el valor de la paraula
tenint en compte els modificadors de les caselles.

Nota: primer s'apliquen els modificadors que afecten només a una lletra i
després els que afecten a tota la paraula.

e) Fes que *ScrabbleEx* implementi *Comparable*.

### Quarta part. Tests.

Crea un programa que creï un vector d'objectes *Scrabble* i un vector de
*String*, i que s'ordenin segons la seva puntuació al joc.

Crea un vector de caselles, i a partir d'ell un objecte *ScrabbleEx* i mostra
el seu valor.