#### A què té accés una subclasse

Gràcies a les possibilitats d'encapsulació del Java, podem decidir
quines característiques d'una classe seran accessibles a les classes
derivades. En general, aquelles parts que formin part de la
implementació més interna de la classe, i que volem que puguin variar
sense afectar a les classes derivades, no haurien de ser accessibles. A
banda de la interfície pública de la classe mare, haurem de fer
accessible a la classe derivada aquells mètodes i atributs pels quals es
pot necessitar que les classes derivades en modifiquin el comportament,
o que es necessiten per mantenir la coherència de les dades quan a la
classe derivada se'n facin modificacions.

Se segueixen les següents regles pel que fa a l'herència:

 * Una subclasse pot accedir a tots els membres (atributs i mètodes) definits a
 la classe mare que siguin *public* o *protected*.

 * Si està al mateix paquet, també té accés als membres *default*.

 * No es té accés als constructors de la classe mare (veure *super*).

Els atributs i mètodes visibles es poden utilitzar directament, com si
s'haguessin definit a la pròpia classe derivada.

Exemple:

```java
public class ClasseBase {
   public int variablePublic;
   protected int variableProtected;
   private int variablePrivate;

   // (...)

   public void metodePublic() {
       System.out.println();
   }

   protected void metodeProtected() {
       System.out.println();
   }

   private void metodePrivate() {
       System.out.println();
   }
}

public class ClasseDerivada extends ClasseBase {
   public void metode() {
       variablePublic = 5; // correcte
       variableProtected = 5; // correcte
       variablePrivate = 5; // incorrecte!!
       metodePublic(); // correcte
       metodeProtected(); // correcte
       metodePrivate(); // incorrecte!!
   }
}
```
