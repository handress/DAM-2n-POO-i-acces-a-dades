#### Exercicis

1. Escriu la sortida del següent programa.

```java
public class Vehicle {
   public Vehicle() {
       System.out.println("S'ha creat un vehicle");
   }
   public void mostraClasse() {
       System.out.println("Classe Vehicle");
   }
   public void mostraTipus() {
       System.out.println("Classe Vehicle");
   }
}
public class Cotxe extends Vehicle {
   public Cotxe() {
       System.out.println("S'ha creat un cotxe");
   }
   public void mostraClasse() {
       System.out.println("Classe Cotxe");
   }
   public void mostraTipus() {
       super.mostraTipus();
       System.out.println("Classe Cotxe");
   }
}
public class Ambulancia extends Cotxe {
   public Ambulancia() {
       System.out.println("S'ha creat una ambulància");
   }
   public void mostraTipus() {
       super.mostraTipus();
       System.out.println("Classe Ambulancia");
   }
}
public class Programa {
   public static void main(String args[]) {
       Vehicle v;
       v = new Vehicle();
       v.mostraClasse();
       v.mostraTipus();
       v = new Cotxe();
       v.mostraClasse();
       v.mostraTipus();
       v = new Ambulancia();
       v.mostraClasse();
       v.mostraTipus();
   }
}
```

2. Escriu quina serà la sortida del següent programa:

```java
class Base {
   public Base() {
       System.out.println("Objecte de classe Base");
   }
   public void mostraClasse1() {
       System.out.println("Classe Base");
   }
   public void mostraClasse2() {
       System.out.println("Classe Base");
   }
}
class Derivada1 extends Base {
   public Derivada1() {
       System.out.println("Objecte de classe Derivada1");
   }
   public void mostraClasse1() {
       System.out.println("Classe Derivada1");
   }
   public void mostraClasse2() {
       super.mostraClasse2();
       mostraClasse1();
   }
}
class Derivada2 extends Base {
   public Derivada2() {
       System.out.println("Objecte de classe Derivada2");
   }
   public void mostraClasse1() {
       System.out.println("Classe Derivada2");
   }
}
public class Programa {
   public static void main(String args[])  {
       Base b;
       Derivada1 d1;
       Derivada2 d2;
       b = new Base();
       b.mostraClasse1();
       d1 = new Derivada1();
       d1.mostraClasse2();
       b = new Derivada2();
       b.mostraClasse1();
       b.mostraClasse2();
       d2 = (Derivada2) b;
       d2.mostraClasse1();
   }
}
```
