## Lectura de fitxers XML amb *SAX*

Amb aquest altre sistema de recórrer fitxers XML el fitxer mai no està sencer
a memòria.

Es fa un recorregut que passa per totes les branques del XML una vegada i,
per cada element es generen dos esdeveniments (un quan s'obre l'etiqueta i un
altre quan es tanca). Podem programar la resposta a aquests esdeveniments per
tal de realitzar un processat del document.

El sistema *SAX* és ideal per recórrer fitxers enormes sense ocupar molta
memòria, però no permet tornar enrere o navegar arbitràriament per qualsevol
branca.

Per utilitzar *SAX* hem de crear una classe derivada de *DefaultHandler*.
Aquesta classe té una colla de mètode que es cridaran cada cop que es produeixi
un cert esdeveniment.

Per exemple, el mètode *startElement()* es cridarà cada cop que es llegeixi
una etiqueta XML que obri un nou element.

Després, utilitzarem un *XMLParser* per recórrer tot el document. A mida que
es vagin llegir els diversos nodes, s'aniran cridant els mètodes del *handler*
que hem programat.

El següent exemple ens mostra per pantalla els esdeveniment que es van produint
a mida que es va llegint el fitxer de mascotes que hem creat abans.

[Classe LectorSAX](codi/fitxers/exemple_xml_sax/src/exemple_xml_sax/LectorSAX.java)

[Classe ElMeuControlador](codi/fitxers/exemple_xml_sax/src/exemple_xml_sax/ElMeuControlador.java)

Analitzem el codi línia a línia:

***Fitxer LectorSAX.java***

**Línia 13**: creem el lector de XML.

**Linia 14**: indiquem quin serà l'objecte que rebrà els esdeveniments que es
produeixin.

**Línia 15**: creem el fluxe d'entrada i el processem. Aquí és on es llegeix
tot el fitxer i des d'on es van cridant els diversos mètodes de
*ElMeuControlador*.

***Fitxer ElMeuControlador.java***

**Línia 7**: el controlador SAX sempre ha d'estendre *DefaultHandler*.

**Línies 8 a 13**: aquest atribut i aquest mètode s'utilitzen per visualitzar
els resultats de forma tabulada. L'atribut *tab* guarda el nivell d'identació
que hem acumulat, i el mètode *tabula()* escriu els espais necessaris.

**Línies 17 a 21**: aquest mètode es crida quan es detecta l'inici del
document XML.

**Línies 25 a 29**: aquest mètode es crida quan es detecta el final del
document XML.

**Línia 33**: aquest mètode es crida cada cop que s'obre un nou element. Rep
el *namespace*, el nom de l'element (qualificat i senzill), i els atributs que
tingui.

**Línies 37 a 42**: recorrem tots els atributs d'aquest element i en mostrem
el nom i el valor.

**Línies 49 a 54**: mètode que es crida quan s'arriba a l'etiqueta que tanca
un element.

**Línia 58**: mètode que es crida quan es troba text. Rep un array de
caracters i la posició d'inici i longitud que indiquen els caracters que s'han
llegit. L'array de caracters conté realment un segment gran del fitxer XML
que s'està llegint, així que saber la posició on llegir és imprescindible
per recuperar el text.

**Línies 60 i 61**: recuperem el tros de text necessari, eliminem els
tabuladors i salts de línia, i els possibles espais al principi i al final.
