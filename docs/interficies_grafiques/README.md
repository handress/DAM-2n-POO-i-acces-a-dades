**POO i accés a dades**

M3UF5. POO. Biblioteques de classes fonamentals
===============================================

- [Introducció](introduccio.md)
- [Scene Builder](scene_builder.md)

Exemples senzills
-----------------

- [Hola món](hola_mon.md)
- [Exemple de binding](codi/interficies_grafiques/javafxBindingExample)
- [Exemple de binding 2](codi/interficies_grafiques/javafxBindingExample2)

Enllaços externs
----------------

- [Tutorial de JavaFX](http://code.makery.ch/library/javafx-8-tutorial/)
- [Documentació de les API de JavaFX](https://docs.oracle.com/javase/8/javafx/api/index.html)
- [Tutorial dels layouts disponibles al JavaFX](http://docs.oracle.com/javase/8/javafx/layout-tutorial/index.html)

Exercicis
---------

- [Exercici Observer Pattern](exercici_observer.md)
- [Exercici quatre en ratlla](exercicis/quatre_en_ratlla.md)
- [Exercici xinos](exercicis/xinos.md)
