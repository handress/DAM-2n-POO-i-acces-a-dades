## Introducció a les interfícies gràfiques en Java

Des que el Java va aparèixer a mitjan dels noranta, la forma d'implementar
interfícies gràfiques ha anat evolucionant. Per tal de mantenir la
compatibilitat amb versions anteriors, les classes originals no s'han
eliminat de les API, de manera que ara mateix conviuen diversos sistemes de
finestres:

 * **AWT** (*Abstract Window Toolkit*): és el conjunt de original proposat pels
 creadors de Java per a la construcció d'interfícies gràfiques. Com que
 Swing es va crear a partir d'ampliar la base que oferia l'AWT (sovint les
 classes de SWing són derivades directament de les seves classes equivalents
 d'AWT), algunes de les classes de l'AWT encara s'utilitzen si es treballa
 en Swing.

 * **Swing**: fins fa poc era el conjunt d'eines recomanat per Oracle per a
 implementar interfícies gràfiques en Java. A diferència de l'AWT, les classes
 de Swing estan implementades en Java i són, per tant, independents del
 sistema operatiu en què s'executen.

 * **JavaFX**: la plataforma JavaX és un altre conjunt de classes per a crear
 interfícies gràfiques en Java. El JavaFX incorpora elements com la
 capacitat d'executar-se en un navegador web. A més suporta animacions i
 gràfics tridimensionals, i es pot executar en sistemes mòbils com Android,
 iOS i Raspberry Pi, entre d'altres.

 Inicialment, JavaFX es distribuïa de forma separada al JRE/JDK. Però a
 partir de la versió 8 de Java, ja forma part de les API estàndard. A més,
 Oracle recomana que els nous programes es desenvolupin en JavaFX en comptes
 de Swing. Encara que es preveu que Swing serà suportat durant molt de
 temps, Oracle ha deixat clar que qualsevol nou desenvolupament es
 realitzaria exclusivament a JavaFX.

 * **SWT** (*Standard Widget Toolkit*): aquest és un conjunt d'eines per a
 construir interícies gràfiques que no està inclòs a la JRE/JDK, però que
 mereix especial atenció degut a què hi ha projectes prou importants que
 l'utilitzen, entre d'ells, Eclipse (que són els que mantenen SWT
 actualment) i diversos projectes d'IBM (que en són els creadors originals).

 El projecte *RAP* (*Remote Application Platform*) uneix SWT amb altres
 tecnologies per al desenvolupament d'aplicacions en xarxa.
