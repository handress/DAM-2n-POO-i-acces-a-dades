## Exercicis

Aquests exercicis utilitzen la base de dades
[Sakila](https://dev.mysql.com/doc/index-other.html). Pots veure aquí la
seva [estructura](https://dev.mysql.com/doc/sakila/en/sakila-structure.html).

### Consultes estàtiques

#### Exercici 1

Fes un programa que mostri el nom i descripció de totes les pel·lícules que
duren més de dues hores i mitja.

#### Exercici 2

Fes un programa que mostri el nom i cognom dels actors que apareixen a la
pel·lícula TWISTED PIRATES.

#### Exercici 3

Fes un programa que mostri quins ítems i de quines botigues té en lloguer el
client de nom ALLISON STANLEY. També haurà de mostrar la data de retorn
d'aquests ítems.

#### Exercici 4

Fes un programa que mostri totes les botigues que hi ha, amb les seves adreces
i el nom i cognom del seu encarregat.

#### Exercici 5

Fes un programa que localitzi els clients que tenen pel·lícules que ja haurien
d'haver retornat. Volem el nom i cognom i el telèfon dels clients i el títol
de la pel·lícula que deuen.

#### Exercici 6

Fes un programa que mostri les diferents categories de pel·lícules que tenim
i quantes pel·lícules de cada categoria hi ha.

### Consultes amb paràmetres

Aquests exercicis s'han de resoldre utilitzant sentències preparades.

#### Exercici 1

Fes un programa que demani a l'usuari el cognom d'un actor i que mostri per
pantalla totes les seves pel·lícules.

#### Exercici 2

Modifica l'exercici 3 per demanar a l'usuari el nom i cognom del client que
s'ha de mostrar.

#### Exercici 3

Fes un programa que permeti cercar un client i ens mostri tots els pagaments
que ha fet entre dues dates donades. De cada pagament es mostrarà la
quantitat, la data, i el nom de l'ítem que s'havia llogat (si és el cas,
hi ha pagaments que no estan associats a cap ítem).

#### Exercici 4

Fes un programa que permeti fer cerques de pel·lícules per paraula clau. El
programa ha de cercar la paraula clau en el títol i en la descripció.

Per cada pel·lícula trobada, s'ha d'indicar a quines botigues es pot trobar.

#### Exercici 5

Fes un programa que permeti als usuaris consultar les pel·lícules
disponibles en una botiga. Primer, es mostraran totes les botigues, amb la
seva adreça.

Un cop seleccionada una botiga, l'usuari podrà realitzar cerques pel nom de
les pel·lícules, pel nom dels actors o pel gènere de les pel·lícules.

El resultat d'aquestes cerques mostrarà el títol de les pel·lícules d'aquesta
botiga que compleixin els requisits, i si estan disponibles, o si no ho
estan, la seva data de retorn.

A través de la llista de pel·lícules, se'n pot seleccionar una, i se'n
mostraran els detalls: títol, descripció, any de producció, nacionalitat
i actors que hi apareixen.

Separa la interacció amb l'usuari de la gestió de la base de dades, de manera
que aquest codi sigui reaprofitable.

### Sentències de modificació de dades

#### Exercici 1

Importa el fitxer [onomastica.txt](onomastica.txt) al teu SGBD de MySQL o
MariaDB.

Observaràs que s'ha creat una nova base de dades anomenada *onomastica*, la
qual conté una única taula anomenada *noms2012*.

L'objectiu d'aquest exercici es fer una aplicació que sigui capaç de llegir
el fitxer [noms_nascuts_2012.csv](noms_nascuts_2012.csv)' i que, tot seguit,
insereixi cadascuna de les línies de què consta com a registres a la taula
*noms2012*.

Ho farem de tres formes diferents (pots gestionar-ho a partir d'un menú que
permeti triar l'opció):

1. Fent servir un objecte *Statement* per a executar una consulta d'inserció
per a cada línia del fitxer *.csv*.

2. Fent servir un objecte *PreparedStatement* per a executar una consulta
d'inserció per a cada línia del fitxer *.csv*.

3. Inserint tots els valors en una única consulta SQL del tipus :

```sql
INSERT INTO noms2012
VALUES (1,'MARC','H',1125,14.59), (2,'JÚLIA/JULIA','M',954,12.37), (...)
```

En aquest últim cas és recomanable que facis servir un objecte *StringBuilder*
en lloc d'*String*.

Fes a més que el programa calculi el temps que triga en inserir les dades en
cada cas i que mostri aquest resultat per pantalla.

A més, afegeix una quarta opció al menú que permeti esborrar completament
la taula *noms2012*.

### Metadades

#### Exercici 1

Fes un programa que donat el nom d'un taula de la base de dades utilitzi
un objecte *DatabaseMetaData* per mostrar les claus primàries d'aquesta
taula, les claus primàries importades per aquesta taula, i les claus
primàries exportades per aquesta taula.

#### Exercici 2

Fes un programa que permeti a l'usuari escriure una sentència *SELECT*.
Aquesta sentència s'executarà contra el servidor MySQL i el seu resultat (o
l'error que es produeixi) es mostrarà per pantalla.

Per mostrar els resultats, cal mostrar una capçalera amb el nom de cada
columna retornada.

### Transaccions

#### Exercici 1

Fes un programa que demani les dades d'un nou empleat i que
l'insereixi a la base de dades *employees*.

El programa demanarà:

- El nom i cognom de l'empleat, i el seu gènere.
- El nom del departament al qual s'ha d'assignar.
- El seu títol inicial.
- El seu sou inicial.

Abans de fer la inserció, cal comprovar que les dades són vàlides:

- El nom del departament ha d'existir a la taula departaments.
- El sou de l'empleat ha de ser positiu.

El programa ha de fer les següents modificacions a la base de dades:

- Afegir una fila a la taula *employees* amb les dades de l'empleat. La data
de contractació és la data actual en el moment d'executar el programa.
- Afegir una fila a la taula *dept_emp* per assignar el nou empleat al seu
departament.
- Afegir una fila a la taula *salaries* per assignar-li el seu sou.
- Afegir una fila a la taula *titles* per assignar-li el seu títol.

S'ha de garantir que les modificacions s'apliquin totes o no se n'apliqui
cap utilitzant una transacció.

### Entorn gràfic i bases de dades

L'aplicació *javafx* que hem vist és funcional però hi ha molts aspectes que
es poden millorar.

Els següents exercicis suggereixen algunes d'aquestes possibilitats.

#### Exercici 1

Ara mateix no podem veure en quin idioma estan fetes les pel·lícules, només
el seu identificador.

Com que la taula *languages* és senzilla i té poques dependències, optarem
per recuperar directament el nom dels idiomes i guardar-los a *Film*, en
comptes de guardar l'id.

Modifica el programa per fer que es vegi bé el nom de l'idioma d'una
pel·lícula i que l'usuari el pugui modificar. No és necessari que es puguin
introduir idiomes nous a la base de dades, en tenim prou amb poder utilitzar
els idiomes que ja hi ha.

#### Exercici 2

El camp de descripció d'una pel·lícula acostuma a ser més llarg que el que
ens permet la visualització en un *TextField*. Modifica la visualització
d'aquest camp per utilitzar un *TextArea* en comptes d'un *TextField*.

#### Exercici 3

En el programa hem deixat de banda el camp del *rating* de les pel·lícules.

Aquest camp es pot tractar de dues maneres: com un *enum* de Java, o com una
cadena.

Decideix una de les dues formes de tractar-ho i modifica el programa per tal
que es vegi aquest camp correctament i l'usuari el pugui modificar.

#### Exercici 4

En aquests moments no es comprova la validesa dels camps abans d'intentar
modificar o afegir una pel·lícula.

Afegir el control d'errors per a tots els camps és molta feina, però ho
podem fer per algun per veure què implica. Un bon camp per experimentar és
l'any de producció de la pel·lícula.

Modifca el programa per tal que només es puguin introduir anys vàlids i
s'informi a l'usuari si intenta afegir o modificar una pel·lícula amb
un any incorrecte. Considerem anys vàlids qualsevol nombre entre el 1900 i
el 2200.

#### Exercici 5

Els nombres que representen quantitats monetàries amb decimals són sempre
delicats: si els guardem en *float* o *double*, degut al límit de precisió,
ens podem trobar que una operació faci que la quantitat de decimals sigui
superior a 2, i després, amb l'arrodoniment, estiguem obtenint quantitats de
diners invàlides.

En aquests casos sovint és millor tractar-les com a quantitas enteres i
treballar amb cèntims en comptes de en euros o dòlars.

Modifica almenys una d'aquestes quantitats per fer que es guardi com a *int*
dins de la classe *Film*. La visualització per part de l'usuari i la
informació de la base de dades no s'ha de veure afectada.

#### Exercici 6

Navegar per una llista de 1000 pel·lícules no és pràctic. Modifica la
interfície gràfica per tal d'afegir un camp de cerca que ens permeti
seleccionar directament una pel·lícula pel seu identificador.

#### Exercici 7

Com a ampliació de la funcionalitat de l'exercici anterior, fes que es
puguin cercar pel·lícules pel seu nom.

### Convocatòria extraordinària

#### Exercici 1

Fes un programa que permeti crear i modificar els clients del nostre vídeoclub.

El programa ha de ser capaç de:

- Crear nous clients, amb l'adreça corresponent.
- Cercar clients existents per id, nom o email i permetre actualitzar qualsevol
de les seves dades.
- Activar o desactivar clients. Els clients del vídeoclub mai s'eliminen de la
base de dades. En comptes d'això hi ha un camp *active* que indica si un
client està actiu o no.
